ARG RUBY_VERSION
ARG ALPINE_VERSION
FROM ruby:${RUBY_VERSION}-alpine${ALPINE_VERSION}

ARG BUNDLER_VERSION
ARG PYTHON_VERSION=python3

RUN apk add --no-cache --update \
  tzdata \
  build-base \
  busybox \
  ca-certificates \
  gcompat \
  libc6-compat \
  curl \
  curl-dev \
  git \
  graphicsmagick \
  libffi-dev \
  libsodium-dev \
  openssh-client \
  postgresql-dev \
  postgresql-client \
  mysql-dev \
  rsync \
  ${PYTHON_VERSION} \
  yarn

RUN gem install bundler -v "${BUNDLER_VERSION}"
